package it.unibo.oop.lab.collections2;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * Instructions
 * 
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 * 
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 * 
 * @param <U> Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

	/*
	 * 
	 * [FIELDS]
	 * 
	 * Define any necessary field
	 * 
	 * In order to save the people followed by a user organized in groups, adopt a
	 * generic-type Map:
	 * 
	 * think of what type of keys and values would best suit the requirements
	 */

	Map<String, LinkedList<U>> groupListMap;

	/*
	 * [CONSTRUCTORS]
	 * 
	 * 1) Complete the definition of the constructor below, for building a user
	 * participating in a social network, with 4 parameters, initializing:
	 * 
	 * - firstName - lastName - username - age and every other necessary field
	 * 
	 * 2) Define a further constructor where age is defaulted to -1
	 */
	public SocialNetworkUserImpl(String firstName, String lastName, String username, int age) {
		super(firstName, lastName, username, age);
		this.groupListMap = new HashMap<String, LinkedList<U>>();
	}

	public SocialNetworkUserImpl(String firstName, String lastName, String username) {
		super(firstName, lastName, username, -1);
		this.groupListMap = new HashMap<String, LinkedList<U>>();
	}

	/**
	 * Builds a new {@link SocialNetworkUserImpl}.
	 * 
	 * @param name    the user firstname
	 * @param surname the user lastname
	 * @param userAge user's age
	 * @param user    alias of the user, i.e. the way a user is identified on an
	 *                application
	 */

	/*
	 * [METHODS]
	 * 
	 * Implements the methods below
	 */

	@Override
	public boolean addFollowedUser(final String circle, final U user) {
		if (groupListMap.get(circle) == null) {
			groupListMap.put(circle, new LinkedList<U>());
		}
		for (U userFor : groupListMap.get(circle)) {
			if (userFor.equals(user)) {
				return false;
			}
		}
		groupListMap.get(circle).add(user);
		return true;
	}

	@Override
	public Collection<U> getFollowedUsersInGroup(final String groupName) {
		List<U> FollowedUsersInGroup = new LinkedList<U>();
		if (groupListMap.get(groupName) == null) {
			groupListMap.put(groupName, new LinkedList<U>());
		}
		for (U userFor : groupListMap.get(groupName)) {
			FollowedUsersInGroup.add(userFor);
		}
		return FollowedUsersInGroup;
	}

	@Override
	public List<U> getFollowedUsers() {
		List<U> totUserList = new LinkedList<U>();
		for (String groupName : groupListMap.keySet()) {
			for (U u : groupListMap.get(groupName)) {
				totUserList.add(u);
			}
		}
		return totUserList;
	}
}
