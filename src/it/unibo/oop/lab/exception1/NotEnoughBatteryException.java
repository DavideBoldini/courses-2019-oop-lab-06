package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String getMessage() {
        return "Battery is empty!!";
    }
	
	

}