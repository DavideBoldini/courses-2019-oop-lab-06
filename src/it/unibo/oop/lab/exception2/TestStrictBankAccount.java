package it.unibo.oop.lab.exception2;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    	AccountHolder a1 = new AccountHolder("Pippo", "Baudo", 1);
    	AccountHolder a2 = new AccountHolder("Carlo", "Conti", 2);
    	
    	StrictBankAccount sb1 = new StrictBankAccount(a1.getUserID(), 10000, 10);
    	StrictBankAccount sb2 = new StrictBankAccount(a2.getUserID(), 10000, 10);
    	
    	sb1.deposit(a1.getUserID(), 500);
    	//Test Exception WrongAccountHolder
    	//sb1.deposit(a2.getUserID(), 100);

    	sb2.depositFromATM(a2.getUserID(), 200);
    	
    	//Test Exception NotEnoughFoundsException
    	//sb2.withdrawFromATM(a2.getUserID(), 1000000);
    	
    	/* Test Exception TransactionsOverQuotaException
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	sb1.depositFromATM(a1.getUserID(), 1);
    	*/
    }
}
