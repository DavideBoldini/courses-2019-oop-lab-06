package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String getMessage() {
        return "Wrong account!!";
    }

	
}
