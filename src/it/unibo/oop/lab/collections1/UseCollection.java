package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

	private UseCollection() {
	}

	/**
	 * @param s unused
	 */
	public static void main(final String... s) {
		/*
		 * 1) Create a new ArrayList<Integer>, and populate it with the numbers from
		 * 1000 (included) to 2000 (excluded).
		 */

		final ArrayList<Integer> listaInt = new ArrayList<Integer>();

		for (int i = 1000; i < 2000; i++) {
			listaInt.add(i);
		}

		/*
		 * 2) Create a new LinkedList<Integer> and, in a single line of code without
		 * using any looping construct (for, while), populate it with the same contents
		 * of the list of point 1.
		 */

		final LinkedList<Integer> listaLinkIntegers = new LinkedList<Integer>();
		listaLinkIntegers.addAll(listaInt);

		/*
		 * 3) Using "set" and "get" and "size" methods, swap the first and last element
		 * of the first list. You can not use any "magic number". (Suggestion: use a
		 * temporary variable)
		 */

		int temp = listaInt.get(listaInt.size() - 1);
		listaInt.set(listaInt.size() - 1, listaInt.get(0));
		listaInt.set(0, temp);

		/*
		 * 4) Using a single for-each, print the contents of the arraylist.
		 */

		for (Integer integer : listaInt) {
			System.out.println(integer);
		}
		/*
		 * 5) Measure the performance of inserting new elements in the head of the
		 * collection: measure the time required to add 100.000 elements as first
		 * element of the collection for both ArrayList and LinkedList, using the
		 * previous lists. In order to measure times, use as example
		 * TestPerformance.java.
		 */
		
		
		long timeArrayList = System.nanoTime();

		for (int i = 0; i < 1_000_00; i++) {
			listaInt.add(0, i);
		}
		
		timeArrayList = System.nanoTime() - timeArrayList;
		
		System.out.println("Tempo impiegato ArrayList (ADD 100.000 items): " + timeArrayList);
		
		/*---------------------------------------------------------------------------------------------------------------*/
		
		long timeLinkedList = System.nanoTime();

		for (int i = 0; i < 1_000_00; i++) {
			listaLinkIntegers.add(0, i);
		}
		
		timeLinkedList = System.nanoTime() - timeLinkedList;
		System.out.println("Tempo impiegato LinkedList (ADD 100.000 items): " + timeLinkedList);

		
		
		/*
		 * 6) Measure the performance of reading 1000 times an element whose position is
		 * in the middle of the collection for both ArrayList and LinkedList, using the
		 * collections of point 5. In order to measure times, use as example
		 * TestPerformance.java.
		 */
		
		
		
		/*
		 * 7) Build a new Map that associates to each continent's name its population:
		 * 
		 * Africa -> 1,110,635,000
		 * 
		 * Americas -> 972,005,000
		 * 
		 * Antarctica -> 0
		 * 
		 * Asia -> 4,298,723,000
		 * 
		 * Europe -> 742,452,000
		 * 
		 * Oceania -> 38,304,000
		 */
		
		final Map<String, Long> continentiPopMap = new HashMap<String, Long>();
		
		continentiPopMap.put("Africa", 1110635000L);
		continentiPopMap.put("Americas", 972005000L);
		continentiPopMap.put("Antarctica", 0L);
		continentiPopMap.put("Asia", 4298723000L);
		continentiPopMap.put("Europe", 742452000L);
		continentiPopMap.put("Oceania", 38304000L);
		
		
		/*
		 * 8) Compute the population of the world
		 */
		
		ArrayList<Long> popArrayList = new ArrayList<Long>(continentiPopMap.values());
		
		long total = 0;
		for (long longs : popArrayList) {
			total = total + longs;			
		}
		System.out.println("TOT Populations: " + total);
	}
}
