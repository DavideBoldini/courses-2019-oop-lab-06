package it.unibo.oop.lab06.generics1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphImpl<N> implements Graph<N> {

	private final Map<N, Set<N>> graphMap;

	public GraphImpl() {
		this.graphMap = new HashMap<N, Set<N>>();
	}

	@Override
	public void addNode(N node) {
		if (!graphMap.containsKey(node)) {
			graphMap.put(node, new HashSet<N>());
		}
	}

	@Override
	public void addEdge(N source, N target) {
		if (source != null && target != null) {
			if (graphMap.containsKey(source)) {
				linkedNodes(source).add(target);
			}
		}
	}

	@Override
	public Set<N> nodeSet() {
		return graphMap.keySet();
	}

	@Override
	public Set<N> linkedNodes(N node) {
		return graphMap.get(node);
	}

	@Override
	public List<N> getPath(N source, N target) {
		List<N> pathList = new LinkedList<N>();
		pathList.add(source);
		Iterator<N> iterator = linkedNodes(source).iterator();
		N elemN = null;
		while (elemN != target) {
			elemN = iterator.next();
			pathList.add(elemN);
			iterator = linkedNodes(elemN).iterator();
		}
		return pathList;
	}
}
